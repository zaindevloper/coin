# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# Category.create(name: 'Bittrex')


 admin = Admin::User.where(email:"benny.grahn@jobbkraft.se").first_or_create(password: "password" , password_confirmation: "password", first_name: "Benny", last_name: "Grahn")
 admin = Admin::User.where(email:"marcus.ekstrom@jobbkraft.se").first_or_create(password: "password" , password_confirmation: "password", first_name: "marcus", last_name: "Ekstrom")
 admin = Admin::User.where(email:"zain@gmail.com").first_or_create(password: "password" , password_confirmation: "password", first_name: "zain", last_name: "ali")
