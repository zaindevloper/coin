class CreateChannels < ActiveRecord::Migration[5.0]
  def change
    create_table :channels do |t|
      t.string :name
      t.string :description
      t.integer :room_id
      t.integer :status , default: 0
      t.string :image_url

      t.timestamps
    end
  end
end
