class CreateMessages < ActiveRecord::Migration[5.0]
  def change
    create_table :messages do |t|
      t.text    :message_text
      t.string  :messageable_type
      t.integer :messageable_id
      t.boolean :is_read
      t.integer :created_by_id

      t.timestamps
    end
  end
end
