class CreateUserChannels < ActiveRecord::Migration[5.0]
  def change
    create_table :user_channels do |t|
      t.integer :user_id
      t.integer :channel_id
      t.integer :status , default: 0

      t.timestamps
    end
  end
end
