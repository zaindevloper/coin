class AddColumnIntoMessageFor < ActiveRecord::Migration[5.0]
  def change
    add_column :messages , :unread_message_by_user , :text ,   array: true, default: []
  end
end
