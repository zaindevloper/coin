class CreateExchanges < ActiveRecord::Migration[5.0]
  def change
    create_table :exchanges do |t|
      t.string :name
      t.text :api_key
      t.text :secret_key
      t.integer :user_id
      t.integer :category_id

      t.timestamps
    end
  end
end
