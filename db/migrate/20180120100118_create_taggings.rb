class CreateTaggings < ActiveRecord::Migration[5.0]
  def change
    create_table :taggings do |t|
      t.integer :user_id
      t.integer :message_id
      t.integer :channel_id
      t.integer :room_id
      t.boolean :is_read , default: false
      t.timestamps
    end
  end
end
