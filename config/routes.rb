Rails.application.routes.draw do


  resources :taggings
  resources :user_channels
  resources :messages
  # devise_for :users, class_name: "Admin::User"

  mount ActionCable.server => '/cable'

  scope '/admin' do

    devise_for :admin_users,
               :class_name  => "Admin::User",
               :path        => '',
               :controllers => {
                   :sessions      => "admin/sessions",
                   :invitations => 'admin/invitations',
               }
    get '/' => 'admin/homes#dashboard'
  end



  namespace :admin  do
    resources :channels
    resources :rooms
  end

  root 'home#index'
  devise_for :users, :controllers => { :omniauth_callbacks => "users/omniauth_callbacks" }

  resources :categories
  resources :exchanges
  resources :channels do
    member do
    get 'update_channel_message'
    end
  end

  resources :messages
  resources :rooms
  resources :conversations do
    member do
      get 'update_conversation_message'
    end

  end

end
