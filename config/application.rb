require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Coinrebels
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    config.to_prepare do
      Devise::SessionsController.layout "login_signup"
      Devise::RegistrationsController.layout "login_signup"
      Devise::ConfirmationsController.layout "login_signup"
      Devise::UnlocksController.layout "login_signup"
      Devise::PasswordsController.layout "login_signup"
    end

    config.assets.paths << "#{Rails.root}/app/assets/fonts"

    config.filestack_rails.api_key = "AM5hQSqJ1S66oyWiZ16MFz"
    config.filestack_rails.client_name = "client"

  end
end
