class Conversation < ApplicationRecord

  has_many :messages, as: :messageable

  validates :first_user_id, :uniqueness => {:scope => :second_user_id}
  belongs_to :first_user, :class_name => 'User', :foreign_key => 'first_user_id'
  belongs_to :second_user , :class_name => 'User', :foreign_key => 'second_user_id'
  scope :first_or_second, -> (first_id , second_id) { where(first_user_id: first_id , second_user_id: second_id) }
  validate :uniqueness_of_conversation

  def uniqueness_of_conversation
    if Conversation.where(first_user_id: self.first_user_id , second_user_id: self.second_user_id ).present? || Conversation.where(first_user_id: self.second_user_id , second_user_id: self.first_user_id ).present?
    errors.add(:first_user_id, "some error")
    end
  end

  def conversation_reciver(user_id)
    if self.first_user.id == user_id
    return second_user
    else
      return first_user
    end
  end

end