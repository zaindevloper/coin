class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable , :omniauthable, :omniauth_providers => [:facebook]

  has_many :exchanges
  has_many :user_channels
  has_many :channels , through: :user_channels
  has_many :taggings

  validates :user_name, presence: true,  on: :update
  validates :user_name, uniqueness: true, on: :update

  after_create :set_user_name
  after_create :set_conversations

  def full_name
    "#{self.first_name} #{self.last_name}"
  end

  def set_conversations
    User.all.each do |user|
      next if user == self
      Conversation.create(name: "#{user.full_name}_#{self.full_name}" , first_user_id: self.id , second_user_id: user.id)
    end
  end

  def set_user_name
    self.update_columns(user_name: find_unique_username(generate_username()) )
  end

  def self.from_omniauth(auth)
    where(provider: auth.provider, uid: auth.uid).first_or_create do |user|
      user.email = auth.info.email
      user.password = Devise.friendly_token[0,20]
      user.first_name = auth.info.first_name
      user.last_name = auth.info.last_name
      user.gender = auth.extra.raw_info.gender
      user.image_url = auth.info.image.sub('http:','https:') # assuming the user model has an image
      # If you are using confirmable and the provider(s) you use validate emails,
      # uncomment the line below to skip the confirmation emails.
      # user.skip_confirmation!
    end
  end

  def is_user_channel?(channel_id)
    if self.channel_ids.include? channel_id
      return true
    else
      return false
    end
  end

  def self.new_with_session(params, session)
    super.tap do |user|
      if data = session["devise.facebook_data"] && session["devise.facebook_data"]["extra"]["raw_info"]
        user.email = data["email"] if user.email.blank?
      end
    end
  end

  private

  def generate_username()
    ActiveSupport::Inflector.transliterate(self.full_name)
        .downcase
        .strip
        .gsub(/[^a-z]/, '_')
        .gsub(/\A_+/, '')
        .gsub(/_+\Z/, '')
        .gsub(/_+/, '_')
  end

  def find_unique_username(username)
    taken_usernames = User.where("user_name LIKE ?", "#{username}%").pluck(:user_name)
    return username if ! taken_usernames.include?(username)
    count = 2
    while true
      # username_2, username_3...
      new_username = "#{username}_#{count}"
      return new_username if ! taken_usernames.include?(new_username)
      count += 1
    end
  end


end
