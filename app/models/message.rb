class Message < ApplicationRecord

  belongs_to :messageable, polymorphic: true
  belongs_to :created_by  , class_name: "User" ,  foreign_key: "created_by_id"

  scope :unread_message ,-> filter {where(" ? = ANY (unread_message_by_user)",  "#{filter}" )}
  default_scope { order("created_at DESC") }

  after_create_commit { MessageBroadcastJob.perform_now(self) }
  after_create :set_unread_message_by_user , if: Proc.new { self.messageable_type == "Channel" }
  after_create :set_all_message_as_read

  def set_unread_message_by_user
    self.update_columns(unread_message_by_user: self.messageable.users.all.pluck(:id)- [self.created_by_id])
  end

  after_create :check_tag_if_present_create_tag , if: Proc.new { self.messageable_type == "Channel" }

  def check_tag_if_present_create_tag
    self.messageable.users.each do |user|
      if self.message_text.split(' ').include?("@#{user.user_name}")
        Tagging.create(user_id: user.id , message_id: self.id , room_id: self.messageable.room.id , channel_id: self.messageable.id)
      end
    end
  end

  def set_all_message_as_read
    # Message::ReadAllUnreadMessage.perform_later(self)
  end

  def message_receiver
    if self.messageable.first_user == self.created_by
      return self.messageable.second_user
    else
      return self.messageable.first_user
    end
  end

end


