class Tagging < ApplicationRecord

  belongs_to :user
  belongs_to :message

  after_create_commit { TaggingBroadcastJob.perform_now(self) }

  scope :room_tagging, -> room_id { where(:room_id =>  room_id)}
  scope :channel_tagging, -> channel_id { where(:channel_id =>  channel_id)}


  scope :un_read, ->  { where(:is_read => false )}



end
