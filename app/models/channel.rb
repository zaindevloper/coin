class Channel < ApplicationRecord

  belongs_to :room , optional: true
  has_many :messages, as: :messageable

  has_many :user_channels
  has_many :users , through: :user_channels

  validates :name, :presence => true
  validates :description, :presence => true
  validates :image_url, :presence => true

  enum status: [:active, :archived ]

end
