class Room < ApplicationRecord

  has_many :channels

  enum status: [ :active, :archived ]
  accepts_nested_attributes_for :channels, reject_if: :all_blank, allow_destroy: true

  validates :name, :presence => true
  validates :description, :presence => true
  validates :image_url, :presence => true

  after_create :create_general_channel_for_room

  def create_general_channel_for_room
    Channel.create(name: "general" , description: "This is a general Channel" , image_url: "https://cdn.filestackcontent.com/8T0UhvUyTByH5WPRIbYu" , room_id: self.id)
  end


  def  unread_channel_message_count(user_id)

    count = 0
    self.channels.each do |channel|
    count = channel.messages.unread_message(user_id).count + count
    end

    return count
  end



end
