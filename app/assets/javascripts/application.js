// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//


//= require common/jquery.min.js
//= require jquery_ujs
//= require common/mobile-detect.min.js
//= require common/perfect-scrollbar.jquery.min.js
//= require common/index.js
//= require common/bootstrap.min.js
//= require common/d3.min.js
//= require common/nv.d3.min.js
//= require application_layout/jquery.dataTables.min.js
//= require application_layout/dataTables.responsive.js
//= require common/pnotify.custom.min.js
//= require common/fuse-html.min.js
//= require common/main.js
//= require cocoon
//= require common/bootstrap-typeahead
//= require common/mention
//= require common/jquery.visible
//= require common/jquery.emojipicker.js
//= require common/jquery.emojis.js

//= require cable




