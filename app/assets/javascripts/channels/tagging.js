App.message = App.cable.subscriptions.create("TaggingChannel", {
    connected: function() {
        // Called when the subscription is ready for use on the server
    },

    disconnected: function() {
        // Called when the subscription has been terminated by the server
    },

    received: function(data) {

        if ($("#"+data.channel_visible_id).length <= 0  ) {

            $("#"+data.room_id).css("visibility", "visible");
            $("#"+data.channel_id).css("visibility", "visible");

            $("#"+data.room_id).css("display", "inline");
            $("#"+data.channel_id).css("display", "inline");

            var sound = document.getElementById("audio");
            sound.play();

        }

    }
});

