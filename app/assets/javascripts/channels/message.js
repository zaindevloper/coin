App.message = App.cable.subscriptions.create("MessageChannel", {
  connected: function() {
    // Called when the subscription is ready for use on the server
  },

  disconnected: function() {
    // Called when the subscription has been terminated by the server
  },

  received: function(data) {

      if ($("#"+data.channel_id).length > 0 && $("#"+data.user_id).length <= 0 ) {

          if ($(".last_message").visible(true) ) {
              $(".last_message").removeClass('last_message');
              $('#chat_append_div').append(data.connent);
              $('#chat_content_scroll').scrollTop($('#chat_content_scroll')[0].scrollHeight);
          }
          else {
              $(".last_message").removeClass('last_message');
              $('#chat_append_div').append(data.connent);
          }

      }
      else if ($("#"+data.channel_id).length <= 0 && $("#"+data.user_id).length <= 0 )
      {
          $("#"+data.channel_count_id).text( parseInt($("#"+data.channel_count_id).text()) + 1);
          $("#"+data.room_id).text( parseInt($("#"+data.room_id).text())+ 1);

      }
  }
});

