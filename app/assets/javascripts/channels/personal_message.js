App.message = App.cable.subscriptions.create("PersonalMessageChannel", {
    connected: function() {
        // Called when the subscription is ready for use on the server
    },

    disconnected: function() {
        // Called when the subscription has been terminated by the server
    },

    received: function(data) {

        if ($("#"+data.conversation_id).length > 0  ) {

            if ($(".last_message").visible(true) )
            {
                $(".last_message").removeClass('last_message');
                $('#chat_append_div').append(data.connent);
                $('#chat_content_scroll').scrollTop($('#chat_content_scroll')[0].scrollHeight);
            }
            else {
                $(".last_message").removeClass('last_message');
                $('#chat_append_div').append(data.connent);
            }
        }
        else if ($("#"+data.conversation_id).length <= 0 )
        {
            $("#"+data.message_count_id).text( parseInt($("#"+data.message_count_id).text()) + 1);
            var sound = document.getElementById("audio");
            sound.play();
        }
    }
});

