json.extract! user_channel, :id, :user_id, :channel_id, :status, :created_at, :updated_at
json.url user_channel_url(user_channel, format: :json)
