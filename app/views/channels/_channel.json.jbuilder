json.extract! channel, :id, :name, :description, :room_id, :status, :image_url, :created_at, :updated_at
json.url channel_url(channel, format: :json)
