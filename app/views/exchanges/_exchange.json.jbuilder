json.extract! exchange, :id, :name, :api_key, :secret_key, :user_id, :category_id, :created_at, :updated_at
json.url exchange_url(exchange, format: :json)
