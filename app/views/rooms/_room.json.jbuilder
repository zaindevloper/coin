json.extract! room, :id, :name, :description, :image_url, :status, :created_at, :updated_at
json.url room_url(room, format: :json)
