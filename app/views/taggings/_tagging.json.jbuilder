json.extract! tagging, :id, :user_id, :message_id, :is_read, :created_at, :updated_at
json.url tagging_url(tagging, format: :json)
