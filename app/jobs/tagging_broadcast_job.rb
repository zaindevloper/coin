class TaggingBroadcastJob < ApplicationJob
  queue_as :high

  def perform(tag)
    if tag.message.messageable_type == "Channel"
      ActionCable.server.broadcast "tagging_for_#{tag.user.id}_channel",
                                   content:  "helo",
                                   channel_id: "tagging_channel#{tag.message.messageable.id}",
                                   channel_visible_id: "channel#{tag.channel_id}",
                                   room_id: "tagging_room#{tag.message.messageable.room.id}"

    end
  end

  private
  def render_message(message)
    MessagesController.render partial: 'messages/append_receiver_message', locals: {message: message}
  end

end
