class Message::ReadAllUnreadMessage < ApplicationJob
  queue_as :default


  def perform(message , current_user)
    @message = message
    @current_user = current_user
    mark_as_read()

  end

  private

  def mark_as_read()
    @message.messageable.messages.unread_message(@current_user).all.each do |message|
      message.update_columns(unread_message_by_user: message.unread_message_by_user - [@current_user.to_s])
    end
  end

end


