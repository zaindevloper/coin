class MessageBroadcastJob < ApplicationJob
  queue_as :high

  def perform(message)

    if message.messageable_type == "Channel"
    ActionCable.server.broadcast "message_for_channel",
                                 content:  "helo",
                                 channel_id: "channel#{message.messageable.id}",
                                 user_id: "user#{message.created_by_id}",
                                 connent: render_message(message),
                                 channel_count_id: "channel_count#{message.messageable.id}",
                                 room_id: "channel_room_id#{message.messageable.room.id}"

    elsif message.messageable_type == "Conversation"
      ActionCable.server.broadcast "message_for_#{message.message_receiver.id}_channel",
                                   content:  "helo",
                                   conversation_id: "conversation#{message.messageable.id}",
                                   connent: render_message(message),
                                   message_count_id: "conversation_count#{message.messageable.id}"


      end
  end

  private

  def render_message(message)
    MessagesController.render partial: 'messages/append_receiver_message', locals: {message: message}
  end
end

