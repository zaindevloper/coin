class UserMailer < ApplicationMailer
  default :from => 'no-reply@coinrebels.com'

  # send a signup email to the user, pass in the user object that   contains the user's email address
  def send_signup_email()

    mail( :to => "zaindeveloper92@gmail.com",
          :subject => 'Thanks for signing up for our amazing app' )
  end
  end