class ApplicationMailer < ActionMailer::Base
  default from: 'coinrebels@.com'
  layout 'mailer'
end
