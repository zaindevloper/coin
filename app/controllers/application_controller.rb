class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  before_filter :prepare_exception_notifier


  before_action :configure_permitted_parameters, if: :devise_controller?


  protected
  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [ :first_name, :last_name , :mobile_number , :user_name])
  end

  private

  def after_sign_out_path_for(resource_or_scope)

    if resource_or_scope == :admin_user
      '/admin'
    else
      new_session_path(resource_or_scope)
    end
  end


  def after_sign_in_path_for(resource)
    if current_admin_user.present?
      '/admin'
    else
      '/'
    end
  end

  def prepare_exception_notifier
    request.env["exception_notifier.exception_data"] = {
        :current_user => current_user
    }
  end




end
