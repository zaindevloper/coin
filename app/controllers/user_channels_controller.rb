class UserChannelsController < ApplicationController
  before_action :set_user_channel, only: [:show, :edit, :update, :destroy]

  def index
    @user_channels = UserChannel.all
  end

  def show
  end

  def new
    @user_channel = UserChannel.new
  end

  def edit
  end

  def create
    @user_channel = UserChannel.new(user_channel_params)
    respond_to do |format|
      if @user_channel.save
        format.html { redirect_to @user_channel, notice: 'User channel was successfully created.' }
        format.json { render :show, status: :created, location: @user_channel }
        format.js {}
      else
        format.html { render :new }
        format.json { render json: @user_channel.errors, status: :unprocessable_entity }
        format.js {}
      end
    end
  end

  def update
    respond_to do |format|
      if @user_channel.update(user_channel_params)
        format.html { redirect_to @user_channel, notice: 'User channel was successfully updated.' }
        format.json { render :show, status: :ok, location: @user_channel }
      else
        format.html { render :edit }
        format.json { render json: @user_channel.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @user_channel.destroy
    respond_to do |format|
      format.html { redirect_to user_channels_url, notice: 'User channel was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def set_user_channel
      @user_channel = UserChannel.find(params[:id])
    end

    def user_channel_params
      params.require(:user_channel).permit(:user_id, :channel_id, :status)
    end
end
