class ChannelsController < ApplicationController
  before_action :set_channel, only: [:show, :edit, :update, :destroy , :update_channel_message]

  # GET /channels
  # GET /channels.json
  def index
    @channels = Channel.all
  end

  # GET /channels/1
  # GET /channels/1.json
  def show

    @unread_message_count = @channel.messages.unread_message(current_user.id).count
    @unread_message_coun = @unread_message_count
    if @unread_message_count != 0
      if @unread_message_count <= 5
        sum_unred = 6 - @unread_message_count
        @unread_message_coun = @unread_message_count + sum_unred
      elsif @unread_message_count >= 6
        @unread_message_coun = @unread_message_count + 1
      end
      @channel_messages  = @channel.messages.paginate(page: params[:page] ,per_page: @unread_message_coun)
    else
      @channel_messages = @channel.messages.paginate(page: params[:page],per_page: 6)
    end
  end

  # GET /channels/new
  def new
    @channel = Channel.new
  end

  # GET /channels/1/edit
  def edit
  end

  # POST /channels
  # POST /channels.json
  def create
    @channel = Channel.new(channel_params)

    respond_to do |format|
      if @channel.save
        format.html { redirect_to @channel, notice: 'Channel was successfully created.' }
        format.json { render :show, status: :created, location: @channel }
      else
        format.html { render :new }
        format.json { render json: @channel.errors, status: :unprocessable_entity }
      end
    end
  end

  def update_channel_message
    if (@channel.messages.present? && @channel.messages.unread_message(current_user.id).count != 0)
      current_user.taggings.channel_tagging(@channel.id).un_read.update_all(is_read: true)
      Message::ReadAllUnreadMessage.perform_now(@channel.messages.first , current_user.id)
    end
  end

  # PATCH/PUT /channels/1
  # PATCH/PUT /channels/1.json
  def update
    respond_to do |format|
      if @channel.update(channel_params)
        format.html { redirect_to @channel, notice: 'Channel was successfully updated.' }
        format.json { render :show, status: :ok, location: @channel }
      else
        format.html { render :edit }
        format.json { render json: @channel.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /channels/1
  # DELETE /channels/1.json
  def destroy
    @channel.destroy
    respond_to do |format|
      format.html { redirect_to channels_url, notice: 'Channel was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_channel
      @channel = Channel.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def channel_params
      params.require(:channel).permit(:name, :description, :room_id, :status, :image_url)
    end
end
