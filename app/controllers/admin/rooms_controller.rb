class Admin::RoomsController < Admin::AdminController
  before_action :set_room, only: [:show, :edit, :update, :destroy]

  def index
    @rooms = Room.all
  end

  def new
    @room = Room.new()
    @room.channels.build
  end

  # POST /channels
  # POST /channels.json
  def create
    @room = Room.new(room_params)
    respond_to do |format|
      if @room.save
        format.html { redirect_to @room, notice: 'Channel was successfully created.' }
        format.json { render :show, status: :created, location: @room }
        format.js {}
      else
        format.html { render :new }
        format.json { render json: @room.errors, status: :unprocessable_entity }
        format.js {}
      end
    end
  end

  def edit

    if @room.channels.empty?
      @room.channels.build
    end

  end

  def update
    respond_to do |format|
      if @room.update(room_params)
        format.html { redirect_to @room, notice: 'Channel was successfully updated.' }
        format.json { render :show, status: :ok, location: @room }
        format.js {}
      else
        format.html { render :edit }
        format.json { render json: @room.errors, status: :unprocessable_entity }
        format.js {}
      end
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_room
    @room = Room.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def room_params
    params.require(:room).permit(:name, :description, :image_url, :status , channels_attributes: [:id, :description, :name,:image_url ,:_destroy])
  end

end